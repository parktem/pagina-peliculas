import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { AppService } from './service/app.service';
import { MovieService } from './service/movie.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'MY-APP';
  showAccess = false;
  notification: any = null;
  loadingBar: boolean = false;

  constructor(private appService: AppService, private movieService: MovieService) {}

  ngOnInit() {
    this.movieService.filmsLoading.subscribe((filmsLoading: boolean) => {
      console.log(filmsLoading);
      if(filmsLoading) {
        this.loadingBar = true;
      } else {
        this.loadingBar = false;
      }
    })

    this.appService.showAccess.subscribe((data: boolean) => {
      this.showAccess = data;
    });

    this.appService.notification.subscribe((notification: {type: string, message: string}) => {
      this.notification = notification;
    });

    firebase.initializeApp({
      apiKey: "AIzaSyAnkyH8peI52bz62pIkPK61io4a3OvlGis",
      authDomain: "paginapeliculas-d7a99.firebaseapp.com"
    });
  }
}
