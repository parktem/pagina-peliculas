export class Film {
  added: boolean;
  adult: string;
  overview: string;
  releaseDate: string;
  voteCount: string;
  id: string;
  video: string;
  voteAverage: string;
  title: string;
  popularity: string;
  posterPath: string;
  originalLanguage: string;
  originalTitle: string;
  genreIds: string[];
  backdropPath: string;

  constructor() {}

}
